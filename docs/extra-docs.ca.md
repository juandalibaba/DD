# Documentació extra

Aquestes configuracions estan automatitzades i només existeixen a títol
informatiu.

## Configuració de Keycloak


Go to `https://sso.DOMINI/auth/admin/master/console`

### THEMES

- [ ] login theme: dd
- [ ] account theme: account-avatar
- [ ] internazionalization enabled: ON
- [ ] default locale: ca


1. Configure -> Realm Settings -> Themes

Configurem d'aquest manera:

![](img/snapshot/1FGGqna.png)

### SECURITY DEFENSES

- [ ] Canviar segona línia de Content-Security-Policy per:
`frame-src 'self'; frame-ancestors 'self' *.DOMAIN localhost; object-src 'none';`

- [ ] La última per:
`max-age=31536000; includeSubDomains`

- [ ] Save

![](img/snapshot/uS5uqJB.png)

### CLIENT SCOPES

- [ ] client scopes => mappers => role_list => Single Role Attribute: ON

![](img/snapshot/Q2i349B.png)

![](img/snapshot/KYbY4ao.png)

![](img/snapshot/oJJPRdp.png)

### CLIENT

- [ ] Clients -> Account-console -> Settings -> Afegir a *Valid Redirect URIs* "https://moodle.DOMINI.net/*" a més de la de wp "https://wp.DOMINI.net/*"

![](img/snapshot/vgamSuC.png)

### EVENTS

![](img/snapshot/events-keycloak.png)


### CLIENTS / account

Afegeix URI de redirecció vàlids

- [ ] `https://moodle.DOMINIDELCENTRE/*`
- [ ] `https://wp.DOMINIDELCENTRE/*`
- [ ] `/realms/master/account/*`
- [ ] `https://nextcloud.DOMINIDELCENTRE/*`

![](img/snapshot/N_42e!m$3Fe.png)

### Configuració Wordpress

![](img/snapshot/Nk8YPCI.png)

![](img/snapshot/3ZRPyzd.png)

Configurar el nickname de Wordpress:
![](img/snapshot/uOwYjOJ.png)

Script: 
```
var Output = user.getFirstName()+" "+user.getLastName();
Output;
```

#### Per a que et permeti tancar sessió de SAML des de Wordpress

![](img/snapshot/myofFZv.png)

Afegim aquests paràmetres:

`/realms/master/account/*`
`https://wp.DOMAIN/*`

![](img/snapshot/7U9t8Zn.png)

Guardem la configuració.

## Configuració NextCloud

### Email
- Per configurar el email: 

![](img/snapshot/5jIt2EE.png)
![](img/snapshot/gMQAKmb.png)


### Cercles

1. Per descarregar els Cercles: Aplicacions -> Aplicacions destacades -> Circles (Descarrega i activa)

![](img/snapshot/yyNyUvc.png)

2. Ara sortirà un menú nou

![](img/snapshot/IbRuJqC.png)

3. Tornem a la pantalla de paràmetres i anem a la secció de Administració -> "Treball en grup" o "Groupware":

![](img/snapshot/yjbOrLz.png)

O bé per linies de comandes:

```
docker exec -u www-data dd-apps-nextcloud-app php occ --no-warnings config:app:set circles members_limit --value="150"
docker exec -u www-data dd-apps-nextcloud-app php occ --no-warnings config:app:set circles allow_linked_groups --value="1"
docker exec -u www-data dd-apps-nextcloud-app php occ --no-warnings config:app:set circles skip_invitation_to_closed_circles --value="1
```

### Altres configuracions

4. Afegir la red de docker com a whitelist. Administració -> Seguretat
![](img/snapshot/9RxNQNx.png)

5. Configurar plantilles OnlyOffice a Nextcloud

![](img/snapshot/ogGM_pzr3ybW.png)
Donar a desar

## Configuració WordPress

### Plugin de Wordpress SAML2


**1. Entrar com admin al WordPress (has d'estar amb la sessió tancada als altres entorns.):  https://wp.\<domain\>/wp-login.php?normal**

**2. Activar el plugin "OneLogin SAML SSO" i aplicar els canvis**


### Configuració de WordPress

Verificar que el plugin GenerateBlock i el tema GeneratePress estan instal·lats i activats. 

![](img/snapshot/gZGNZXY.png)

![](img/snapshot/iThTdIa.png)


- Per configurar la hora i l'idioma

![](img/snapshot/JbyHUqJ.png)
