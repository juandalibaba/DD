# DD

DD és el workspace educatiu generat en el marc del Pla de Digitalització Democràtica d'Xnet. Ha estat creat i powered per
Xnet, famílies i centres promotors, IsardVDI, 3iPunt, MaadiX, eXO.cat, Evilham i financiat per la Direcció d'Innovació
Democràtica, Comissionat d'Innovació Digital, Comissionat d'Economia Social de l'Ajuntament de Barcelona, en
col·laboració amb Consorci d'Educació de Barcelona, aFFaC i AirVPN.

L'aplicació DD pot utilitzar-se lliurement sempre i quan consti aquest footer i es respecti la llicència
[AGPLv3](https://www.gnu.org/licenses/agpl-3.0.en.html).

# Què és DD?

DD configura un proveïdor d'identitat i moltes aplicacions per oferir una
experiència d'usuari cohesiva, considerant escoles i universitats com el
cas d'ús principal.

Aquest projecte proporciona una solució integrada per gestionar entorns
educatius comuns:

- **Aules**: Una instància de Moodle amb tema i connectors personalitzats
- **Fitxers**: Una instància de Nextcloud amb tema i connectors personalitzats
- **Documents**: Un visor i editor de documents integrat amb Nextcloud
- **Pàgines web**: Una instància de WordPress amb tema i connectors personalitzats
- **Pad**: Una instància d'Etherpad integrada amb Nextcloud
- **Conferències**: Un BigBlueButton integrat amb Moodle i Nextcloud (necessita servidor independent)
- **Formularis**: Un connector de NextCloud per formularis

|                              |                                 |
| ---------------------------- | ------------------------------- |
| ![](img/classrooms.png) | ![](img/cloud_storage.png) |

## Interfície d'administració

Aquest projecte inclou una interfície d'administració que permet administrar
fàcilment usuaris i grups per tal de mantenir-los sincronitzats entre les
diverses aplicacions.

| ![](img/admin_sync.png) | ![](img/admin_user_edit.png) |
| ---------------------------- | --------------------------------- |

Per tal de migrar i inserir usuaris i grups al sistema de manera senzilla hi ha
dos camins d'importació:

- Des de la consola d'administració de Google amb un fitxer JSON
- Des d'un fitxer CSV

# M'interessa!

Genial! Tant si vols contribuir com si tens interés en desplegar DD per la teva
organització, ens alegrarà parlar amb tu.
Aquí tens alguns recursos per guiar-te més:

- [Manual d'usuari DD](https://dd.digitalitzacio-democratica.xnet-x.net/manual-usuari/)
- [Instal·lació](install.ca.md)
- [Post-instal·lació](post-install.ca.md)
- [Codi font](https://gitlab.com/DD-workspace/DD)


Aquest web està fet amb [MkDocs](https://gitlab.com/pages/mkdocs).
Podeu [veure i modificar el codi font](https://gitlab.com/DD-workspace/DD).
