ARG IMG
FROM ${IMG}

MAINTAINER James Swineson <jamesswineson@gmail.com>

ENV NODE_ENV production

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y curl unzip mariadb-client supervisor gzip git python libssl-dev pkg-config build-essential && \
    rm -r /var/lib/apt/lists/*

WORKDIR /opt/

ARG ETHERPAD_VERSION

RUN curl -SL \
    https://github.com/ether/etherpad-lite/archive/${ETHERPAD_VERSION}.zip \
    > etherpad.zip && unzip etherpad && rm etherpad.zip && \
    mv etherpad-lite-${ETHERPAD_VERSION} etherpad-lite

WORKDIR etherpad-lite

RUN bin/installDeps.sh \
    && rm settings.json
COPY entrypoint.sh /entrypoint.sh

RUN sed -i 's/^node/exec\ node/' bin/run.sh

VOLUME /opt/etherpad-lite/var
RUN ln -s var/settings.json settings.json
ADD supervisor.conf /etc/supervisor/supervisor.conf

EXPOSE 9001
ENTRYPOINT ["/entrypoint.sh"]
CMD ["supervisord", "-c", "/etc/supervisor/supervisor.conf", "-n"]
