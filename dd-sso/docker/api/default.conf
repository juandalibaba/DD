#
#   Copyright © 2021,2022 IsardVDI S.L.
#
#   This file is part of DD
#
#   DD is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or (at your
#   option) any later version.
#
#   DD is distributed in the hope that it will be useful, but WITHOUT ANY
#   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#   FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
#   details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with DD. If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: AGPL-3.0-or-later
upstream api {
    server 127.0.0.1:7039 fail_timeout=0;
}

server {
    listen       80;
    server_name  localhost;

    root /api/api/static/;
    expires 1m;
    log_not_found off;
    access_log off;

    add_header  Access-Control-Allow-Origin *;

    location /avatar {
        proxy_pass http://127.0.0.1:7039/avatar;
       expires 1m;
        log_not_found off;
        access_log off;
    }

    location /restart {
        proxy_pass http://127.0.0.1:7039/restart;
       expires 1m;
        log_not_found off;
        access_log off;
    }

    location /user_menu/json {
        alias /api/api/static/templates/user_menu_header.json;
        default_type application/json;
        index user_menu_header.json;
       expires 1m;
        log_not_found off;
        access_log off;
    }

    location /user_menu/html {
        alias /api/api/static/templates/user_menu_header.html;
        default_type text/html;
        index user_menu_header.html;
       expires 1m;
        log_not_found off;
        access_log off;
    }

    location /json {
        alias /api/api/static/templates/header.json;
        default_type application/json;
        index header.json;
       expires 1m;
        log_not_found off;
        access_log off;
    }

    location /header/html {
        alias /api/api/static/templates/header.html;
        default_type text/html;
        index header.html;
       expires 1m;
        log_not_found off;
        access_log off;
    }

    location /header/html/nextcloud {
        alias /api/api/static/templates/header_nextcloud.html;
        default_type text/html;
        index header_nextcloud.html
       expires 1m;
        log_not_found off;
        access_log off;
    }

    location /header/html/admin {
        alias /api/api/static/templates/header_admin.html;
        default_type text/html;
        index header_admin.html
       expires 1m;
        log_not_found off;
        access_log off;
    }

    location /header/html/sso {
        alias /api/api/static/templates/header_sso.html;
        default_type text/html;
        index header_sso.html
       expires 1m;
        log_not_found off;
        access_log off;
    }

   location / {
        try_files $uri $uri/
       expires 1m;
        log_not_found off;
        access_log off;
    }
}
