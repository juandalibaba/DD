#
#   Copyright © 2021,2022 IsardVDI S.L.
#
#   This file is part of DD
#
#   DD is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or (at your
#   option) any later version.
#
#   DD is distributed in the hope that it will be useful, but WITHOUT ANY
#   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#   FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
#   details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with DD. If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: AGPL-3.0-or-later

import socket
from functools import wraps

from flask import redirect, request, url_for


def is_internal(fn):
    @wraps(fn)
    def decorated_view(*args, **kwargs):
        remote_addr = (
            request.headers["X-Forwarded-For"].split(",")[0]
            if "X-Forwarded-For" in request.headers
            else request.remote_addr.split(",")[0]
        )
        if socket.gethostbyname("dd-sso-admin") == remote_addr:
            return fn(*args, **kwargs)
        return ""

    return decorated_view
