#
#   Copyright © 2021,2022 IsardVDI S.L.
#
#   This file is part of DD
#
#   DD is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or (at your
#   option) any later version.
#
#   DD is distributed in the hope that it will be useful, but WITHOUT ANY
#   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#   FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
#   details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with DD. If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: AGPL-3.0-or-later
ARG NGINX_ALPINE_IMG
FROM $NGINX_ALPINE_IMG as production
MAINTAINER IsardVDI S.L. <info@isard.com>

RUN apk add --no-cache python3 py3-pip
RUN pip3 install --upgrade pip
RUN apk add --no-cache --virtual .build_deps \
    build-base \
    python3-dev \
    libffi-dev 
COPY ./requirements.pip3 /requirements.pip3
RUN pip3 install --no-cache-dir -r requirements.pip3
RUN apk del .build_deps

RUN apk add curl py3-yaml

COPY ./src /api
RUN wget https://raw.githubusercontent.com/twbs/bootstrap/v4.0.0/dist/css/bootstrap.min.css -O /api/api/static/css/bootstrap.min.css
RUN wget -qO - https://github.com/FortAwesome/Font-Awesome/archive/v4.7.0.zip | busybox unzip -d /api/api/static/css/ -
RUN mv /api/api/static/css/Font-Awesome-4.7.0 /api/api/static/css/font-awesome-4.7.0
#https://fontawesome.com/v4.7/assets/font-awesome-4.7.0.zip

COPY default.conf /etc/nginx/conf.d/default.conf
ADD entrypoint.sh /docker-entrypoint.d/
RUN chmod 775 /docker-entrypoint.d/entrypoint.sh
