#
#   Copyright © 2021,2022 IsardVDI S.L.
#
#   This file is part of DD
#
#   DD is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or (at your
#   option) any later version.
#
#   DD is distributed in the hope that it will be useful, but WITHOUT ANY
#   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#   FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
#   details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with DD. If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: AGPL-3.0-or-later
resolvers mydns
    nameserver dns1 127.0.0.11:53
    
global
#   debug
    daemon
    log             127.0.0.1    local0
    tune.ssl.default-dh-param 2048
    h1-case-adjust content-type Content-Type
    h1-case-adjust content-encoding Content-Encoding
    h1-case-adjust transfer-encoding Transfer-Encoding

  defaults
    mode http
    timeout         connect 120s
    timeout         client 120s
    timeout         client-fin 120s
    timeout         server 120s
    timeout         tunnel 7200s
    option          http-server-close
    option          httpclose
    log             global
    option          httplog
    backlog         4096
    maxconn         2000
    option          tcpka
    option          h1-case-adjust-bogus-client

frontend website
    mode http
    bind :80
    redirect scheme https if !{ env(BEHIND_PROXY) -m str true } !{ ssl_fc }
    http-request del-header ssl_client_cert unless { ssl_fc_has_crt }
    http-request set-header ssl_client_cert -----BEGIN\ CERTIFICATE-----\ %[ssl_c_der,base64]\ -----END\ CERTIFICATE-----\  if { ssl_fc_has_crt }
    bind :443 ssl crt /certs/chain.pem

    acl is_upgrade hdr(Connection) -i upgrade
    acl is_websocket hdr(Upgrade) -i websocket 
    
    acl is_nextcloud hdr_beg(host) nextcloud.
    acl is_moodle hdr_beg(host) moodle. !path_beg -i /local/tresipuntimportgc/
    acl is_moodle_long hdr_beg(host) moodle.  path_beg -i /local/tresipuntimportgc/
    acl is_jitsi hdr_beg(host) jitsi.
    acl is_oof hdr_beg(host) oof.
    acl is_wp hdr_sub(host) .wp.
    acl is_wp hdr_beg(host) wp.
    acl is_pad hdr_beg(host) pad.
    acl is_sso hdr_beg(host) sso.
    acl is_ipa hdr_beg(host) ipa.
    acl is_api hdr_beg(host) api.
    acl is_admin hdr_beg(host) admin.

    acl is_root path -i /
    http-request deny if is_pad is_root

    use_backend be_api if { path_end -i favicon.ico } or { path_end -i favicon } or { path_beg -i /apps/theming/favicon/ }
  
    use_backend letsencrypt if { path_beg /.well-known/acme-challenge/ }
    use_backend be_api if is_nextcloud { path_beg /avatar/ }
    use_backend be_nextcloud if is_nextcloud
    use_backend be_moodle_long if is_moodle_long
    use_backend be_moodle if is_moodle
    use_backend be_jitsi if is_jitsi
    use_backend be_oof if is_oof
    use_backend be_wp if is_wp
    use_backend be_etherpad if is_pad
    use_backend be_admin if is_sso { path_beg /socket.io }
    use_backend be_adminer if is_sso { path_beg /dd-sso-adminer }
    use_backend be_admin if is_admin
    use_backend be_sso if is_sso
    use_backend be_ipa if is_ipa
    use_backend be_api if is_api
    
    http-request redirect code 301 location https://moodle."${DOMAIN}" if { hdr(host) -i "${DOMAIN}" }
#    default_backend be_sso

backend letsencrypt
  server letsencrypt 127.0.0.1:8080

backend be_api
	mode http
  http-request set-path /img/favicon.ico if { path_end -i favicon.ico } or { path_end -i favicon } or { path_beg -i /apps/theming/favicon/ }
  acl existing-x-forwarded-host req.hdr(X-Forwarded-Host) -m found
  acl existing-x-forwarded-proto req.hdr(X-Forwarded-Proto) -m found
  http-request add-header X-Forwarded-Host %[req.hdr(Host)] unless existing-x-forwarded-host
  http-request add-header X-Forwarded-Proto https unless existing-x-forwarded-proto
  # Nextcloud use /avatar/username/32 /avatar/username/64 and /avatar/username/128
  http-request set-path %[path,regsub(\"^(/avatar/[^/]+).*\",\"\1\")]
	server api dd-sso-api:80 check port 80 inter 5s rise 2 fall 10 resolvers mydns init-addr none

backend be_ipa
	mode http
  acl existing-x-forwarded-host req.hdr(X-Forwarded-Host) -m found
  acl existing-x-forwarded-proto req.hdr(X-Forwarded-Proto) -m found
  http-request add-header X-Forwarded-Host %[req.hdr(Host)] unless existing-x-forwarded-host
  http-request add-header X-Forwarded-Proto https unless existing-x-forwarded-proto
	server freeipa dd-sso-freeipa:443 check port 443 ssl verify none inter 5s rise 2 fall 10 resolvers mydns init-addr none

backend be_sso
	mode http
  option httpclose
	#option http-server-close
	option forwardfor
  acl existing-x-forwarded-host req.hdr(X-Forwarded-Host) -m found
  acl existing-x-forwarded-proto req.hdr(X-Forwarded-Proto) -m found
  http-request add-header X-Forwarded-Host %[req.hdr(Host)] unless existing-x-forwarded-host
  http-request add-header X-Forwarded-Proto https unless existing-x-forwarded-proto
  http-response replace-header Set-Cookie (KEYCLOAK_LOCALE=[^;]*);(.*) \1;Domain="${DOMAIN}";Version=1;Path=/;Secure;
	server keycloak dd-sso-keycloak:8080 check port 8080 inter 5s rise 2 fall 10 resolvers mydns init-addr none

backend be_admin
	mode http
  option forwardfor
  timeout queue 600s
  timeout server 600s
  timeout connect 600s
  # acl authorized http_auth(AuthUsers)
  # http-request auth realm AuthUsers unless authorized
  acl existing-x-forwarded-host req.hdr(X-Forwarded-Host) -m found
  acl existing-x-forwarded-proto req.hdr(X-Forwarded-Proto) -m found
  http-request add-header X-Forwarded-Host %[req.hdr(Host)] unless existing-x-forwarded-host
  http-request add-header X-Forwarded-Proto https unless existing-x-forwarded-proto
	server dd-sso-admin dd-sso-admin:9000 check port 9000 inter 5s rise 2 fall 10 resolvers mydns init-addr none

backend be_adminer
	mode http
  # acl authorized http_auth(AuthUsers)
  # http-request auth realm AuthUsers unless authorized
  http-request redirect scheme http drop-query append-slash if { path -m str /dd-sso-adminer }
  http-request replace-path /dd-sso-adminer/(.*) /\1 
  # http-request del-header Authorization 
  acl existing-x-forwarded-host req.hdr(X-Forwarded-Host) -m found
  acl existing-x-forwarded-proto req.hdr(X-Forwarded-Proto) -m found
  http-request add-header X-Forwarded-Host %[req.hdr(Host)] unless existing-x-forwarded-host
  http-request add-header X-Forwarded-Proto https unless existing-x-forwarded-proto
	server dd-sso-adminer dd-sso-adminer:8080 check port 8080 inter 5s rise 2 fall 10 resolvers mydns init-addr none

## APPS
backend be_moodle
	mode http
  acl existing-x-forwarded-host req.hdr(X-Forwarded-Host) -m found
  acl existing-x-forwarded-proto req.hdr(X-Forwarded-Proto) -m found
  http-request add-header X-Forwarded-Host %[req.hdr(Host)] unless existing-x-forwarded-host
  http-request add-header X-Forwarded-Proto https unless existing-x-forwarded-proto
	server moodle dd-apps-moodle:8080 check port 8080 inter 5s rise 2 fall 10 resolvers mydns init-addr none

backend be_moodle_long
	mode http
  timeout server 900s
  acl existing-x-forwarded-host req.hdr(X-Forwarded-Host) -m found
  acl existing-x-forwarded-proto req.hdr(X-Forwarded-Proto) -m found
  http-request add-header X-Forwarded-Host %[req.hdr(Host)] unless existing-x-forwarded-host
  http-request add-header X-Forwarded-Proto https unless existing-x-forwarded-proto
	server moodle dd-apps-moodle:8080 check port 8080 inter 5s rise 2 fall 10 resolvers mydns init-addr none

backend be_nextcloud
	mode http
  acl existing-x-forwarded-host req.hdr(X-Forwarded-Host) -m found
  acl existing-x-forwarded-proto req.hdr(X-Forwarded-Proto) -m found
  http-request add-header X-Forwarded-Host %[req.hdr(Host)] unless existing-x-forwarded-host
  http-request add-header X-Forwarded-Proto https unless existing-x-forwarded-proto
	server nextcloud dd-apps-nextcloud-nginx:80 check port 80 inter 5s rise 2 fall 10 resolvers mydns init-addr none

backend be_etherpad
	mode http
  acl existing-x-forwarded-host req.hdr(X-Forwarded-Host) -m found
  acl existing-x-forwarded-proto req.hdr(X-Forwarded-Proto) -m found
  http-request add-header X-Forwarded-Host %[req.hdr(Host)] unless existing-x-forwarded-host
  http-request add-header X-Forwarded-Proto https unless existing-x-forwarded-proto
	server etherpad dd-apps-etherpad:9001 check port 9001 inter 5s rise 2 fall 10 resolvers mydns init-addr none

backend be_jitsi
	mode http
  acl existing-x-forwarded-host req.hdr(X-Forwarded-Host) -m found
  acl existing-x-forwarded-proto req.hdr(X-Forwarded-Proto) -m found
  http-request add-header X-Forwarded-Host %[req.hdr(Host)] unless existing-x-forwarded-host
  http-request add-header X-Forwarded-Proto https unless existing-x-forwarded-proto
	server jitsi dd-apps-jitsi:80 check port 80 inter 5s rise 2 fall 10 resolvers mydns init-addr none

backend be_oof
	mode http
  acl existing-x-forwarded-host req.hdr(X-Forwarded-Host) -m found
  acl existing-x-forwarded-proto req.hdr(X-Forwarded-Proto) -m found
  http-request add-header X-Forwarded-Host %[req.hdr(Host)] unless existing-x-forwarded-host
  http-request add-header X-Forwarded-Proto https unless existing-x-forwarded-proto
	server onlyoffice dd-apps-onlyoffice:80 check port 80 inter 5s rise 2 fall 10 resolvers mydns init-addr none

backend be_wp
	mode http
  acl existing-x-forwarded-host req.hdr(X-Forwarded-Host) -m found
  acl existing-x-forwarded-proto req.hdr(X-Forwarded-Proto) -m found
  http-request add-header X-Forwarded-Host %[req.hdr(Host)] unless existing-x-forwarded-host
  http-request add-header X-Forwarded-Proto https unless existing-x-forwarded-proto

    http-request set-header X-SSL %[ssl_fc]
    http-request set-header X-Forwarded-Proto https
	server wp dd-apps-wordpress:80 check port 80 inter 5s rise 2 fall 10 resolvers mydns init-addr none


  listen stats 
        bind                0.0.0.0:8888
        mode                http
        stats               enable
        option              httplog
        stats               show-legends
        stats               uri /haproxy
        stats               realm Haproxy\ Statistics
        stats               refresh 5s
        #stats               auth staging:mypassword
        #acl authorized http_auth(AuthUsers)
        #stats          http-request auth unless authorized
        timeout             connect 5000ms
        timeout             client 50000ms
        timeout             server 50000ms

userlist AuthUsers
    user admin password $6$grgQMVfwI0XSGAQl$2usaQC9LVXXXYHtSkGUf74CIGsiH8fi/K.V6DuKSq0twPkmFGP2vL/b//Ulp2I4xBEZ3eYDhUbwBPK8jpmsbo.
