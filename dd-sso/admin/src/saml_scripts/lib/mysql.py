#
#   Copyright © 2021,2022 IsardVDI S.L.
#
#   This file is part of DD
#
#   DD is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or (at your
#   option) any later version.
#
#   DD is distributed in the hope that it will be useful, but WITHOUT ANY
#   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#   FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
#   details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with DD. If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: AGPL-3.0-or-later

import json
import logging as log
import time
import traceback
from datetime import datetime, timedelta

import mysql.connector
import yaml

# from admin import app


class Mysql:
    def __init__(self, host, database, user, password):
        self.conn = mysql.connector.connect(
            host=host, database=database, user=user, password=password
        )

    def select(self, sql):
        self.cur = self.conn.cursor()
        self.cur.execute(sql)
        data = self.cur.fetchall()
        self.cur.close()
        return data

    def update(self, sql):
        self.cur = self.conn.cursor()
        self.cur.execute(sql)
        self.conn.commit()
        self.cur.close()
