//
//   Copyright © 2021,2022 IsardVDI S.L.
//
//   This file is part of DD
//
//   DD is free software: you can redistribute it and/or modify
//   it under the terms of the GNU Affero General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or (at your
//   option) any later version.
//
//   DD is distributed in the hope that it will be useful, but WITHOUT ANY
//   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//   FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
//   details.
//
//   You should have received a copy of the GNU Affero General Public License
//   along with DD. If not, see <https://www.gnu.org/licenses/>.
//
// SPDX-License-Identifier: AGPL-3.0-or-later

$(document).ready(function () {
  init_wysiwyg();

  var lang = getCookie('KEYCLOAK_LOCALE') ? getCookie('KEYCLOAK_LOCALE') : 'ca'
  $('#legal-lang').val(lang)
  getLangLegal(lang)
  // $('#privacy-lang').val(lang)

  // $.ajax({
  //   type: "GET",
  //   url: "/api/legal/privacy",
  //   data: {
  //     lang: lang
  //   },
  //   success: function (data) {
  //     $('#editor-privacy').html(data.html)
  //   }
  // })

  $("#save-legal").click(function () {
    console.log($('#editor-legal').cleanHtml())
    console.log($('#legal-lang').val())
    $.ajax({
      type: "POST",
      url: "/api/legal/legal",
      data: JSON.stringify({
        'html': $('#editor-legal').cleanHtml(),
        'lang': $('#legal-lang').val()
      }),
      success: function () {
        new PNotify({
          title: "Legal text",
          text: "Updated for "+$('#legal-lang').val()+" language",
          hide: true,
          delay: 3000,
          icon: 'fa fa-alert-sign',
          opacity: 1,
          type: 'info'
      });
      },
    });
  });

  $('#legal-lang').on('change', function() {
    getLangLegal(this.value)
  });

  // $("#save-privacy").click(function () {
  //   $.ajax({
  //     type: "POST",
  //     url: "/api/legal/privacy",
  //     data: {
  //       'html': $('#editor-privacy').cleanHtml(),
  //       'lang': $('#legal-lang').val()
  //     },
  //     success: function () {
  //     },
  //   });
  // });
});

function getLangLegal(lang) {
  $.ajax({
    type: "GET",
    url: "/api/legal/legal",
    data: {
      lang: lang
    },
    success: function (data) {
      $('#editor-legal').html(data.html)
    }
  })
}

function getCookie(cname) {
  let name = cname + "=";
  let decodedCookie = decodeURIComponent(document.cookie);
  let ca = decodedCookie.split(';');
  for(let i = 0; i <ca.length; i++) {
    let c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

function init_wysiwyg() {
  $("#editor-legal").wysiwyg({
    toolbarSelector: '[data-target="#editor-legal"]'
  });
  // $("#editor-privacy").wysiwyg({
  //   toolbarSelector: '[data-target="#editor-privacy"]'
  // });
  window.prettyPrint;
  prettyPrint();
}